# Deploy the AWS Load Balancer Controller.
# https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html
#
# The AWS resources needed for this are deployed in the `aws` module.

resource "kubernetes_service_account_v1" "aws_load_balancer_controller" {
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"

    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name"      = "aws-load-balancer-controller"
    }

    annotations = {
      "eks.amazonaws.com/role-arn" = var.load_balancer_controller_role_arn
    }
  }
}

resource "helm_release" "aws_load_balancer_controller" {
  name       = "aws-load-balancer-controller"
  namespace  = "kube-system"
  chart      = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  version    = var.load_balancer_controller_chart_version

  values = [jsonencode({
    clusterName = var.eks_cluster_name
    region      = var.region
    vpcId       = var.vpc_id

    serviceAccount = {
      name   = kubernetes_service_account_v1.aws_load_balancer_controller.metadata[0].name
      create = false
    }

    image = {
      repository = "${local.eks_addon_image_registry}/amazon/aws-load-balancer-controller"
    }
  })]
}
