include "root" {
  path = find_in_parent_folders()
}

dependency "aws" {
  config_path = "../aws"

  # Mock the outputs when using certain commands so that we can run the commands
  # before the `aws` module is applied.
  mock_outputs_allowed_terraform_commands = ["validate", "init", "fmt"]
  mock_outputs = {
    efs_csi_driver_role_arn           = "arn:efs-csi-driver"
    eks_cluster_name                  = "cluster"
    load_balancer_controller_role_arn = "arn:load-balancer-controller"
    sqs_queue_name                    = "queue.fifo"
    sqs_queue_url                     = "https://sqs.example.com/account/queue.fifo"
    vpc_id                            = "vpc-id"
    ytdl_database_uri                 = "postgresql://username:password@hostname:port/database"
    ytdl_role_arn                     = "arn:ytdl"
    ytdl_volume_handle                = "fs-id::fsap-id"
  }
}

inputs = {
  efs_csi_driver_role_arn           = dependency.aws.outputs.efs_csi_driver_role_arn
  eks_cluster_name                  = dependency.aws.outputs.eks_cluster_name
  load_balancer_controller_role_arn = dependency.aws.outputs.load_balancer_controller_role_arn
  sqs_queue_name                    = dependency.aws.outputs.sqs_queue_name
  sqs_queue_url                     = dependency.aws.outputs.sqs_queue_url
  vpc_id                            = dependency.aws.outputs.vpc_id
  ytdl_database_uri                 = dependency.aws.outputs.ytdl_database_uri
  ytdl_role_arn                     = dependency.aws.outputs.ytdl_role_arn
  ytdl_volume_handle                = dependency.aws.outputs.ytdl_volume_handle
}
