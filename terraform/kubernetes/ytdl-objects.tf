# Create the Kubernetes objects that are needed by ytdl-server.
#
# ytdl-server itself is deployed separately via Kustomize.
#
# These objects are created here because they contain dynamic Terraform
# outputs that aren't accessible by Kustomize.

# Namespace that ytdl-server is deployed to.
resource "kubernetes_namespace_v1" "ytdl" {
  metadata {
    name = var.ytdl_namespace
  }
}

# Service account that's used to run ytdl-server.
# The service account uses an IAM role to access the SQS Celery broker.
resource "kubernetes_service_account_v1" "ytdl" {
  metadata {
    name      = "ytdl-server"
    namespace = kubernetes_namespace_v1.ytdl.metadata[0].name

    labels = {
      "app.kubernetes.io/name" = "ytdl-server"
    }

    annotations = {
      "eks.amazonaws.com/role-arn" = var.ytdl_role_arn
    }
  }
}

# Persistent volume that holds the videos downloaded by the worker.
resource "kubernetes_persistent_volume_v1" "ytdl_videos" {
  metadata {
    name = "ytdl-videos"

    labels = {
      "app.kubernetes.io/name"    = "ytdl-videos"
      "app.kubernetes.io/part-of" = "ytdl-server"
    }
  }

  spec {
    access_modes                     = ["ReadWriteMany"]
    volume_mode                      = "Filesystem"
    storage_class_name               = ""
    persistent_volume_reclaim_policy = "Retain"

    capacity = {
      # The size is ignored by EFS.
      storage = "5Gi"
    }

    persistent_volume_source {
      csi {
        driver        = "efs.csi.aws.com"
        volume_handle = var.ytdl_volume_handle
      }
    }
  }
}

# Secret env-var that's used to set the database URI.
resource "kubernetes_secret_v1" "ytdl_environment" {
  type = "Opaque"

  metadata {
    name      = "ytdl-environment"
    namespace = kubernetes_namespace_v1.ytdl.metadata[0].name

    labels = {
      "app.kubernetes.io/name"    = "ytdl-environment"
      "app.kubernetes.io/part-of" = "ytdl-server"
    }
  }

  data = {
    YTDL_DATABASE_URI = var.ytdl_database_uri
  }
}

locals {
  # Default config file for ytdl-server. This is merged with `var.ytdl_config`.
  ytdl_default_config = {
    download_dir = "/videos"
    ytdl_opts = {
      default_opts = {
        cachedir = "/cache"
      }
    }
    celery_config = {
      broker_url = "sqs://"
      broker_transport_options = {
        predefined_queues = {
          (var.sqs_queue_name) = {
            url = var.sqs_queue_url
          }
        }
      }
      # We have to change the default queue because the name of the queue must
      # end with '.fifo' when using a FIFO queue.
      task_default_queue = var.sqs_queue_name
    }
  }

  # Config file for ytdl-server, which will be deployed as a config map.
  ytdl_merged_config = merge(local.ytdl_default_config, var.ytdl_config)
}

# Config map that contains the config file for ytdl-server.
resource "kubernetes_config_map_v1" "ytdl_config" {
  metadata {
    name      = "ytdl-config"
    namespace = kubernetes_namespace_v1.ytdl.metadata[0].name

    labels = {
      "app.kubernetes.io/name"    = "ytdl-config"
      "app.kubernetes.io/part-of" = "ytdl-server"
    }
  }

  data = {
    "ytdl-server.yml" = jsonencode(local.ytdl_merged_config)
  }
}
