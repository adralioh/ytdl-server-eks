# Deploy the NGINX Ingress Controller.
# https://kubernetes.github.io/ingress-nginx/deploy/

resource "kubernetes_namespace_v1" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"
  }
}

resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  namespace  = kubernetes_namespace_v1.ingress_nginx.metadata[0].name
  chart      = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  version    = var.ingress_nginx_chart_version

  values = [jsonencode({
    controller = {
      ingressClassResource = {
        # Make the controller's IngressClass the default IngressClass so that
        # you don't have to specify it explicitly when creating Ingress
        # resources.
        # This is needed when using Kubernetes v1.22+.
        default = true
      }

      service = {
        # Set the annotations on the LoadBalancer service so that it uses an NLB
        # that's automatically deployed by the AWS Load Balancer Controller.
        # https://docs.aws.amazon.com/eks/latest/userguide/network-load-balancing.html
        annotations = {
          "service.beta.kubernetes.io/aws-load-balancer-type"            = "external"
          "service.beta.kubernetes.io/aws-load-balancer-nlb-target-type" = "ip"
          "service.beta.kubernetes.io/aws-load-balancer-scheme"          = "internet-facing"
        }
      }
    }
  })]

  # Terraform is unable to destroy the aws-load-balancer-controller chart if it
  # tries to destroy it before the ingress-nginx chart is destroyed.
  depends_on = [
    helm_release.aws_load_balancer_controller
  ]
}
