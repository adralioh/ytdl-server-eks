# Deploy the AWS EFS CSI Driver.
# https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html
#
# The AWS resources needed for this are deployed in the `aws` module.

resource "kubernetes_service_account_v1" "efs_csi_controller_sa" {
  metadata {
    name      = "efs-csi-controller-sa"
    namespace = "kube-system"

    labels = {
      "app.kubernetes.io/name" = "efs-csi-controller-sa"
    }

    annotations = {
      "eks.amazonaws.com/role-arn" = var.efs_csi_driver_role_arn
    }
  }
}

resource "helm_release" "aws_efs_csi_driver" {
  name       = "aws-efs-csi-driver"
  namespace  = "kube-system"
  chart      = "aws-efs-csi-driver"
  repository = "https://kubernetes-sigs.github.io/aws-efs-csi-driver"
  version    = var.efs_csi_driver_chart_version

  values = [jsonencode({
    controller = {
      serviceAccount = {
        name   = kubernetes_service_account_v1.efs_csi_controller_sa.metadata[0].name
        create = false
      }
    }

    image = {
      repository = "${local.eks_addon_image_registry}/eks/aws-efs-csi-driver"
    }
  })]
}
