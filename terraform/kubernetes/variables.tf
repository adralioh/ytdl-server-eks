variable "region" {
  description = "AWS region. Example: \"us-east-1\"."
  type        = string
}

variable "eks_cluster_name" {
  description = "Name of the EKS cluster."
  type        = string
}

variable "load_balancer_controller_role_arn" {
  description = "ARN of the role that's assumed by the AWS Load Balancer Controller service account."
  type        = string
}

variable "load_balancer_controller_chart_version" {
  description = "Helm chart version of the AWS Load Balancer Controller. This is NOT the app version. The default value corresponds to app version v2.3.1."
  type        = string
  default     = "1.3.3"
}

variable "ingress_nginx_chart_version" {
  description = "Helm chart version of the NGINX Ingress Controller. This is NOT the app version. The default value corresponds to app version v1.1.1."
  type        = string
  default     = "4.0.17"
}

variable "efs_csi_driver_chart_version" {
  description = "Helm chart version of the AWS EFS CSI Driver. This is NOT the app version. The default value corresponds to app version v1.3.6."
  type        = string
  default     = "2.2.3"
}

variable "vpc_id" {
  description = "ID of the VPC that the EKS cluster is located in. Example: \"vpc-xxxxxxxx\""
  type        = string
}

variable "eks_addon_image_registry" {
  description = "EKS add-on image registry URL. By default, the value will be determined automatically using a hardcoded value. You might need to specify it manually if the default value is outdated. See the following for a list of registries: https://docs.aws.amazon.com/eks/latest/userguide/add-ons-images.html"
  type        = string
  default     = null
}

variable "efs_csi_driver_role_arn" {
  description = "ARN of the role that's assumed by the AWS EFS CSI Driver service account."
  type        = string
}

variable "ytdl_volume_handle" {
  description = "Volume handle of the EFS mount point that will be provisioned as a PV. This should be in the format \"[FileSystemId]:[Subpath]:[AccessPointId]\"."
  type        = string
}

variable "ytdl_database_uri" {
  description = "URI of the ytdl-server database."
  type        = string
  sensitive   = true
}

variable "ytdl_namespace" {
  description = "Name of the Kubernetes namespace that will be used to run ytdl-server."
  type        = string
  default     = "ytdl-server"
}

variable "ytdl_role_arn" {
  description = "ARN of the role that's assumed by the ytdl-server service account."
  type        = string
}

variable "sqs_queue_name" {
  description = "Name of the SQS queue, which is used as the Celery broker."
  type        = string
}
variable "sqs_queue_url" {
  description = "URL of the SQS queue, which is used as the Celery broker."
  type        = string
}

variable "ytdl_config" {
  description = "Additional options which will be added to the ytdl-server config file. See the ytdl-server docs for available options."
  type        = map(any)
  default     = {}
}
