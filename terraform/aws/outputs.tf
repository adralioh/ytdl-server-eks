output "region" {
  value       = var.region
  description = "AWS region."
}

output "eks_cluster_name" {
  value       = aws_eks_cluster.this.name
  description = "Name of the EKS cluster."
}
output "eks_cluster_endpoint" {
  value       = aws_eks_cluster.this.endpoint
  description = "Endpoint URL of the EKS API server."
}
output "eks_cluster_certificate_authority_data" {
  value       = aws_eks_cluster.this.certificate_authority[0].data
  description = "Base64-encoded certificate data required to communicate with the EKS cluster."
}

output "load_balancer_controller_role_arn" {
  value       = aws_iam_role.load_balancer_controller.arn
  description = "ARN of the role that's assumed by the AWS Load Balancer Controller."
}

output "efs_csi_driver_role_arn" {
  value       = aws_iam_role.efs_csi_driver.arn
  description = "ARN of the role that's assumed by the AWS EFS CSI Driver."
}

output "ytdl_volume_handle" {
  value       = "${aws_efs_file_system.this.id}::${aws_efs_access_point.this.id}"
  description = "Volume handle for the EFS /videos volume that is used by ytdl-server."
}

output "vpc_id" {
  value       = aws_vpc.this.id
  description = "ID of the VPC that is created."
}

output "ytdl_database_uri" {
  value       = "postgresql://${aws_db_instance.this.username}:${aws_db_instance.this.password}@${aws_db_instance.this.endpoint}/${aws_db_instance.this.db_name}"
  description = "URI of the RDS PostgreSQL database, formatted for use as the ytdl-server database."
  sensitive   = true
}

output "ytdl_role_arn" {
  value       = aws_iam_role.ytdl.arn
  description = "ARN of the role that's assumed by assumed by ytdl-server."
}

output "sqs_queue_name" {
  value       = aws_sqs_queue.this.name
  description = "Name of the SQS queue, which is used as the Celery broker."
}
output "sqs_queue_url" {
  value       = aws_sqs_queue.this.url
  description = "URL of the SQS queue, which is used as the Celery broker."
}
