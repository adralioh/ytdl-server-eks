variable "region" {
  description = "AWS region. Example: \"us-east-1\"."
  type        = string
}

variable "availability_zone_count" {
  description = "Number of availability zones to spread the application over."
  type        = number
  default     = 2

  # EKS and RDS require at least 2 AZs.
  validation {
    condition     = var.availability_zone_count >= 2
    error_message = "Availability zone count must be at least 2."
  }
}

variable "vpc_cidr_block" {
  description = "CIDR block of the VPC."
  type        = string
  default     = "10.0.0.0/20" # 10.0.0.1 - 10.0.15.254
}

variable "subnet_newbits" {
  description = "Number of additional bits with which to extend each subnet. For example, if $vpc_cidr_block has a 20-bit prefix and $subnet_newbits is 5, each subnet will have a 25-bit prefix (20+5)."
  type        = number
  default     = 5 # 25-bit, 128 total addresses
}

variable "eks_node_group_desired_size" {
  description = "Desired size of the auto-scaling EKS node group."
  type        = number
  default     = 2
}

variable "eks_node_group_max_size" {
  description = "Maximum size of the auto-scaling EKS node group."
  type        = number
  default     = 2
}

variable "eks_node_group_min_size" {
  description = "Minimum size of the auto-scaling EKS node group."
  type        = number
  default     = 2
}

variable "eks_node_group_instance_types" {
  description = "List of instance types that will be used in the EKS node group. Note that each instance type has a max number of pods. See https://github.com/awslabs/amazon-eks-ami/blob/master/files/eni-max-pods.txt"
  type        = list(string)
  default     = ["t3.small"]
}

variable "eks_kubernetes_version" {
  description = "Kubernetes version."
  type        = string
  default     = "1.21"
}

variable "db_username" {
  description = "Username of the master user of the RDS PostgreSQL database."
  type        = string
  default     = "postgres"
}

variable "db_password" {
  description = "Password of the master user of the RDS PostgreSQL database."
  type        = string
  sensitive   = true
}

variable "db_instance_class" {
  description = "Instance class of the RDS PostgreSQL database."
  type        = string
  default     = "db.t3.micro"
}

variable "db_engine_version" {
  description = "Version of the RDS PostgreSQL database."
  type        = string
  default     = "14.1"
}

variable "db_allocated_storage" {
  description = "Allocated storage in gibibytes of the RDS PostgreSQL database."
  type        = number
  default     = 20
}

variable "db_storage_type" {
  description = "Storage type of the RDS PostgreSQL database."
  type        = string
  default     = "gp2"
}

variable "ytdl_namespace" {
  description = "Name of the Kubernetes namespace that will be used to run ytdl-server."
  type        = string
  default     = "ytdl-server"
}

variable "ytdl_uid" {
  description = "UID that is used to run ytdl-server within the container. This is needed so that we can set the proper permissions on the EFS /videos volume"
  type        = number
  default     = 235000
}
