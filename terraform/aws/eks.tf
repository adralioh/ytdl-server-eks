# Create the EKS cluster.
# https://docs.aws.amazon.com/eks/latest/userguide/getting-started-console.html

resource "aws_eks_cluster" "this" {
  name     = "ytdl"
  role_arn = aws_iam_role.eks_cluster.arn
  version  = var.eks_kubernetes_version

  # TODO: Consider creating dedicated /28 subnets for the cluster ENIs:
  #       https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
  # TODO: Consider enabling the private cluster endpoint:
  #       https://docs.aws.amazon.com/eks/latest/userguide/cluster-endpoint.html
  vpc_config {
    subnet_ids = aws_subnet.private[*].id
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [aws_iam_role_policy_attachment.eks_cluster]
}

resource "aws_eks_node_group" "this" {
  cluster_name    = aws_eks_cluster.this.name
  node_group_name = "ytdl"
  node_role_arn   = aws_iam_role.eks_node.arn
  subnet_ids      = aws_subnet.private[*].id
  instance_types  = var.eks_node_group_instance_types
  version         = var.eks_kubernetes_version

  scaling_config {
    desired_size = var.eks_node_group_desired_size
    max_size     = var.eks_node_group_max_size
    min_size     = var.eks_node_group_min_size
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.eks_node_worker_node_policy,
    aws_iam_role_policy_attachment.eks_node_container_registry_read_only,
    aws_iam_role_policy_attachment.eks_node_cni_policy
  ]
}

# Create the role that's attached to the EKS cluster
data "aws_iam_policy_document" "eks_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "eks_cluster" {
  name               = "eks-cluster"
  assume_role_policy = data.aws_iam_policy_document.eks_assume_role_policy.json
}
resource "aws_iam_role_policy_attachment" "eks_cluster" {
  role       = aws_iam_role.eks_cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

# Create the role that's attached to the managed EC2 node group
data "aws_iam_policy_document" "eks_node_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "eks_node" {
  name               = "eks-node"
  assume_role_policy = data.aws_iam_policy_document.eks_node_assume_role_policy.json
}
resource "aws_iam_role_policy_attachment" "eks_node_worker_node_policy" {
  role       = aws_iam_role.eks_node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}
resource "aws_iam_role_policy_attachment" "eks_node_container_registry_read_only" {
  role       = aws_iam_role.eks_node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
resource "aws_iam_role_policy_attachment" "eks_node_cni_policy" {
  role       = aws_iam_role.eks_node.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

# An IAM OIDC provider is required for service accounts in the cluster to use IAM roles.
# https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html
resource "aws_iam_openid_connect_provider" "this" {
  url             = aws_eks_cluster.this.identity[0].oidc[0].issuer
  thumbprint_list = [data.tls_certificate.eks_cluster_oidc.certificates[0].sha1_fingerprint]
  client_id_list  = ["sts.amazonaws.com"]
}
data "tls_certificate" "eks_cluster_oidc" {
  url = aws_eks_cluster.this.identity[0].oidc[0].issuer
}
