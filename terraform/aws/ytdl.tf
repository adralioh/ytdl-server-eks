# Create the roles and policies that are needed by the ytdl-server service
# account in EKS.

data "aws_iam_policy_document" "ytdl_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.this.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "${aws_iam_openid_connect_provider.this.url}:sub"
      values   = ["system:serviceaccount:${var.ytdl_namespace}:ytdl-server"]
    }
  }
}

resource "aws_iam_role" "ytdl" {
  name               = "ytdl-serviceaccount"
  description        = "Allow the ytdl-server Kubernetes service-account to access SQS"
  assume_role_policy = data.aws_iam_policy_document.ytdl_assume_role_policy.json
}

data "aws_iam_policy_document" "sqs" {
  statement {
    effect    = "Allow"
    resources = [aws_sqs_queue.this.arn]

    actions = [
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:SendMessage"
    ]
  }
}

resource "aws_iam_policy" "sqs" {
  name        = "ytdl-sqs"
  description = "Allow access to the ytdl-server SQS queue for use as a Celery broker"
  policy      = data.aws_iam_policy_document.sqs.json
}

resource "aws_iam_role_policy_attachment" "ytdl_sqs" {
  role       = aws_iam_role.ytdl.name
  policy_arn = aws_iam_policy.sqs.arn
}
