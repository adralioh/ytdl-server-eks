# Create the roles and policies that are needed for the AWS Load Balancer
# Controller to work within the EKS cluster.
# The controller itself is deployed in the `kubernetes` module.
#
# https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html

data "aws_iam_policy_document" "load_balancer_controller_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.this.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "${aws_iam_openid_connect_provider.this.url}:sub"
      values   = ["system:serviceaccount:kube-system:aws-load-balancer-controller"]
    }
  }
}

resource "aws_iam_role" "load_balancer_controller" {
  name               = "aws-load-balancer-controller-ytdl"
  description        = "Allow the AWS Load Controller Balancer to make calls to AWS APIs for the ytdl EKS cluster"
  assume_role_policy = data.aws_iam_policy_document.load_balancer_controller_assume_role_policy.json
}

resource "aws_iam_policy" "load_balancer_controller" {
  name        = "aws-load-balancer-controller"
  description = "Allow the AWS Load Controller Balancer to make calls to AWS APIs"
  # The policy file is bundled from AWS Load Balancer Controller v2.3.1, which
  # is licensed under the Apache License v2.0.
  # Source:
  #   https://github.com/kubernetes-sigs/aws-load-balancer-controller/blob/v2.3.1/docs/install/iam_policy.json
  policy = file("${path.module}/files/aws-load-balancer-controller-iam-policy.json")

  tags = {
    controller-version = "v2.3.1"
  }
}

resource "aws_iam_role_policy_attachment" "load_balancer_controller" {
  role       = aws_iam_role.load_balancer_controller.name
  policy_arn = aws_iam_policy.load_balancer_controller.arn
}
