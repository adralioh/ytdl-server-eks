# Deploy the RDS PostgreSQL database.

resource "aws_db_subnet_group" "private" {
  name        = "ytdl"
  description = "ytdl-server private subnets"
  subnet_ids  = aws_subnet.private[*].id

  tags = {
    Name = "ytdl-private"
  }
}

# TODO: Add an option to make the database multi-AZ.
resource "aws_db_instance" "this" {
  db_name                = "ytdl"
  engine                 = "postgres"
  engine_version         = var.db_engine_version
  instance_class         = var.db_instance_class
  storage_type           = var.db_storage_type
  allocated_storage      = var.db_allocated_storage
  username               = var.db_username
  password               = var.db_password
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.private.name
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = {
    Name = "ytdl"
  }
}

resource "aws_security_group" "rds" {
  name        = "ytdl-rds"
  description = "Allow access to the ytdl RDS database"
  vpc_id      = aws_vpc.this.id

  ingress {
    description = "Allow PostgreSQL from the ytdl private subnets"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = aws_subnet.private[*].cidr_block
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ytdl-rds"
  }
}
