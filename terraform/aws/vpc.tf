# Create the VPC and subnets that everything else will be deployed to.

data "aws_availability_zones" "all" {}

locals {
  # Sort the list of AZs so that the same AZs are always used.
  availability_zones = sort(data.aws_availability_zones.all.names)
}

resource "aws_vpc" "this" {
  cidr_block         = var.vpc_cidr_block
  enable_dns_support = true
  # DNS hostnames must be enabled for EKS to work.
  enable_dns_hostnames = true

  tags = {
    Name = "ytdl"
  }
}

# Create a public subnet for each AZ.
resource "aws_subnet" "public" {
  count = var.availability_zone_count

  vpc_id            = aws_vpc.this.id
  availability_zone = local.availability_zones[count.index]
  # Only use even values for `netnum` so that we can use the odd values for the private subnets.
  # This prevents the CIDR blocks from completely changing when `var.subnet_newbits` changes.
  cidr_block              = cidrsubnet(aws_vpc.this.cidr_block, var.subnet_newbits, count.index * 2)
  map_public_ip_on_launch = true

  tags = {
    Name = "ytdl-public-${count.index + 1}"
    # Allow EKS to deploy public ELBs to these subnets.
    "kubernetes.io/role/elb" = "1"
  }
}

# Create a private subnet for each AZ.
resource "aws_subnet" "private" {
  count = var.availability_zone_count

  vpc_id            = aws_vpc.this.id
  availability_zone = local.availability_zones[count.index]
  # Only use odd values for `netnum` so that we can use the even values for the public subnets.
  cidr_block              = cidrsubnet(aws_vpc.this.cidr_block, var.subnet_newbits, count.index * 2 + 1)
  map_public_ip_on_launch = false

  tags = {
    Name = "ytdl-private-${count.index + 1}"
    # Allow EKS to deploy internal ELBs to these subnets.
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id

  tags = {
    Name = "ytdl-gateway"
  }
}

# Create a NAT gateway in each public subnet.
resource "aws_eip" "nat" {
  count = var.availability_zone_count

  vpc = true
}
resource "aws_nat_gateway" "this" {
  count = var.availability_zone_count

  allocation_id = aws_eip.nat[count.index].id
  subnet_id     = aws_subnet.public[count.index].id

  tags = {
    Name = "ytdl-gateway-${count.index + 1}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = {
    Name = "ytdl-public-route-table"
  }
}
resource "aws_route_table_association" "public" {
  count = var.availability_zone_count

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

# Create a separate route table for each private subnet so that they route to
# the correct NAT gateway.
resource "aws_route_table" "private" {
  count = var.availability_zone_count

  vpc_id = aws_vpc.this.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this[count.index].id
  }

  tags = {
    Name = "ytdl-private-route-table-${count.index + 1}"
  }
}
resource "aws_route_table_association" "private" {
  count = var.availability_zone_count

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}
