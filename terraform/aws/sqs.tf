# Create the SQS queue, which is used as the Celery broker
resource "aws_sqs_queue" "this" {
  name       = "ytdl-broker.fifo"
  fifo_queue = true
}
