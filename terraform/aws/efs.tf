# Set up EFS so that it can be used for volumes within the EKS cluster.
# The CSI driver is deployed in the `kubernetes` module.
#
# https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html

# TODO: Do something with the videos after they're downloaded to reduce cost.
#       Potential options:
#         - Transition them to EFS IA.
#         - Upload the videos to S3 after the job completes by using the
#           ExecAfterDownload postprocessor.
resource "aws_efs_file_system" "this" {
  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"

  tags = {
    Name = "ytdl-videos"
  }
}

# Create an EFS mount target in each private subnet.
resource "aws_efs_mount_target" "this" {
  count = var.availability_zone_count

  file_system_id  = aws_efs_file_system.this.id
  subnet_id       = aws_subnet.private[count.index].id
  security_groups = [aws_security_group.efs.id]
}

# Create an access point for ytdl-server so that it can access the /videos
# volume as a non-root user.
resource "aws_efs_access_point" "this" {
  file_system_id = aws_efs_file_system.this.id

  posix_user {
    uid = var.ytdl_uid
    gid = var.ytdl_uid
  }

  root_directory {
    path = "/videos"

    creation_info {
      owner_uid   = var.ytdl_uid
      owner_gid   = var.ytdl_uid
      permissions = "0775"
    }
  }
}

resource "aws_security_group" "efs" {
  name        = "ytdl-efs"
  description = "Allow access to the ytdl EFS file system"
  vpc_id      = aws_vpc.this.id

  ingress {
    description = "Allow NFS from the ytdl private subnets"
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = aws_subnet.private[*].cidr_block
  }

  tags = {
    Name = "ytdl-efs"
  }
}

# Create the role and policy needed for the EFS CSI Driver
data "aws_iam_policy_document" "efs_csi_driver_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.this.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "${aws_iam_openid_connect_provider.this.url}:sub"
      values   = ["system:serviceaccount:kube-system:efs-csi-controller-sa"]
    }
  }
}
resource "aws_iam_role" "efs_csi_driver" {
  name               = "aws-efs-csi-driver-ytdl"
  description        = "Allow the AWS EFS CSI driver to manage EFS volumes for the ytdl EKS cluster."
  assume_role_policy = data.aws_iam_policy_document.efs_csi_driver_assume_role_policy.json
}
resource "aws_iam_policy" "efs_csi_driver" {
  name        = "aws-efs-csi-driver"
  description = "Allow the AWS EFS CSI driver to manage EFS volumes for EKS."
  # The policy file is bundled from Amazon EFS CSI Driver v1.3.6, which is
  # licensed under the Apache License v2.0.
  # Source:
  #   https://github.com/kubernetes-sigs/aws-efs-csi-driver/blob/v1.3.6/docs/iam-policy-example.json
  policy = file("${path.module}/files/aws-efs-csi-driver-iam-policy.json")

  tags = {
    driver-version = "v1.3.6"
  }
}
resource "aws_iam_role_policy_attachment" "efs_csi_driver" {
  role       = aws_iam_role.efs_csi_driver.name
  policy_arn = aws_iam_policy.efs_csi_driver.arn
}
