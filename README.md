# ytdl-server-eks
ytdl-server-eks is a set of Terraform modules and Kubernetes manifests for
running [ytdl-server](https://gitlab.com/adralioh/ytdl-server) in an Amazon EKS
cluster.

## Requirements
- [Terraform](https://www.terraform.io/)
- [Terragrunt](https://terragrunt.gruntwork.io/)
- [aws-cli](https://aws.amazon.com/cli/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
- [Helm](https://helm.sh/)

## Structure
![ytdl-server-eks network diagram](diagram.drawio.svg)

SQS is used as the Celery broker, and RDS is used as the
database/result-backend.

Videos are downloaded to an EFS share.

For ingress, an NLB is deployed by
[AWS Load Balancer Controller][aws-load-balancer-controller]. This NLB forwards
traffic to [ingress-nginx][ingress-nginx], which then forwards traffic to the
ytdl-server API service.

[aws-load-balancer-controller]: https://github.com/kubernetes-sigs/aws-load-balancer-controller
[ingress-nginx]: https://github.com/kubernetes/ingress-nginx

> The reason that ingress-nginx with an NLB was chosen instead of just using an
  ALB is because ingress-nginx supports basic auth.

Multiple AZs are used for redundancy (minimum 2). A public and private subnet
are created in each AZ, and the EKS cluster is deployed across them.

## Configuration
### Terraform
See `terraform/aws/variables.tf` and `terraform/kubernetes/variables.tf` for a
list of Terraform variables that you can change.

You can change the variables by adding them to the file
`terraform/terragrunt.hcl`.

### Kubernetes
You can override ytdl-server Kubernetes attributes, such as the number of
replicas or environment variables, by using [Kustomize][kustomize].

See `kubernetes/basic-auth/` for an example kustomization overlay that adds
basic auth, and see [The Kustomization File][kustomization-file] for a list of
directives supported by Kustomize.

[kustomize]: https://kubectl.docs.kubernetes.io/guides/introduction/kustomize/
[kustomization-file]: https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/

If you create a new kustomization overlay, be sure to use it in place of `base`
when running kubectl in the following sections.

## Deployment
1.  Deploy all the AWS resources and set up the EKS cluster.
    ```bash
    # PWD: terraform
    terragrunt run-all apply
    ```
1.  Update your kubeconfig file so that you can access the EKS cluster using
    kubectl.
    ```bash
    # PWD: terraform/aws
    aws eks update-kubeconfig \
        --region="$(terragrunt output -raw region)" \
        --name="$(terragrunt output -raw eks_cluster_name)"
    ```
1.  Deploy ytdl-server into the EKS cluster.

    By default, the ytdl-server REST API will be publicly accessible. If you
    want to protect it with Basic Authentication, replace `base` with
    `basic-auth`.
    ```bash
    # PWD: kubernetes

    # Publicly accessible
    kubectl apply -n ytdl-server -k base
    # Basic auth
    kubectl apply -n ytdl-server -k basic-auth
    ```
1.  Run the following command to get the hostname that the ytdl-server REST API
    is accessible at.
    ```bash
    kubectl get -n ytdl-server ingress api
    ```

## Cleanup
1.  Delete ytdl-server from the EKS cluster.
    ```bash
    # PWD: kubernetes

    # Publicly accessible
    kubectl delete -n ytdl-server -k base
    # Basic auth
    kubectl delete -n ytdl-server -k basic-auth
    ```
1.  Destroy the EKS cluster along with all the other AWS resources.
    ```bash
    # PWD: terraform
    terragrunt run-all destroy
    ```
